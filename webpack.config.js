/*
Webpack pakker all .js i prosjketet etter transkompilering av Bable, eksterne bibliotek etc inn i Bundle.js
*/


/*
Arg:
entry: Forteller webpack hvor den skal starte å prosessere js for prosjketet
output: Forteller hvor webpack skal putte resultatet fra kompileringen (__dirname er en lokal kommando i Node.js)

resolve: Bestemmer hvilke filtyper webpack vil prosessere
  Root: Root for node app
  Alias: Brukes for å gi globale alias til lokale komponenter. Eks import Nav from ''/components/Nav.jsx'; ->  import Nav from 'Nav';

extentions: Sier hvilke filtyper som skal forventes. import Nav from ''/components/Nav.jsx'; -> import Nav from ''/components/Nav';

loaders: Eksterne plugins som benyttes i kompileringen. Her bruker vi babel, med es2015 og stage-0. Dette inkluderer de mest eksperimentalle funktionene til es2015 som object decompositioning
  test: Babel forventer .js.  /\.jsx?$/ spesifiserer via et regex at vi skal se etter .jsx også.
  exclude: ekskluderer prosessering av filer i node_modules og bower_components

*/
module.exports = {
  entry: './app/app.jsx',
  output: {
    path: __dirname,
    filename: './public/bundle.js'

  },
  resolve:{
    root: __dirname,
    alias:{
    Nav: './components/Nav.jsx',
    WelcomeText:  './components/WelcomeText.jsx',
    Search: './components/Search.jsx'

    },
    extensions: ['','.js','.jsx']
  },
  module:{
    loaders:[
      {
        loader: 'babel-loader',
        query:{
          presets:['react', 'es2015', 'stage-0']
        },
        test:/\.jsx?$/,
        exclude: /(node_modules|bower_components)/
      }
    ]
  }
};
