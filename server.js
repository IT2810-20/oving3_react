/*
Express er en light weight server som kjøer på toppen av Node.
Server.js starter opp en lokal server.

Start: Node server.js
Aksesspunkt:  Server kjører på localhost:3000

express.static('mappe'): Tjener alt som er i mappen til brukere som sender spørringer til serveren

app.listen(port, callback): Setter opp port for hvor serveren lytter etter spørringer. Callback blir kalt ved sukessfull oppstart

*/

var express = require("express");

//initierer express
var app = express();

app.use(express.static('public'));

app.listen(3000, function(){
  console.log("Express server kjører på port 3000");
});
