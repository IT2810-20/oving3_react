var React = require('react');

const WelcomeText = () =>{
  //class er et reservert ord i jsx, CSS class byttes derfor ut med "className" i jsx
  return(
    <div className="hero-text-box">
        <h1>Hvilke fag tar du? </h1>
        <h3>Spar penger ved å kjøpe billige bøker </h3>
    </div>
  );
};

//export default: Tenk på det som en "return" statement
export default WelcomeText;
