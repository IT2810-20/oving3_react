//Link fra react router hjelper med state dependent styling
var {Link} = require('react-router');
var React = require('react');




const Nav = () =>{
  /*
    Semantisk bruk av container tags, <header>, <nav>, for bedre kodefortåelse over <div>. Hjelper i tillegg de med funksjonsnedsettelser.
    class er et reservert ord i jsx, så CSS class byttes derfor ut med "className"
    <a></a> byttes ut med <link></link> i React. Denne gir ekstra fordeler over HTML med bedre kontroll over styling som tar hensyn til State
  */
  return(
    <header>
      <nav>
          <div className="row">
              <img src="./res/img/logo3.png" alt="Zen logo" className="logo"></img>
              <ul className="main-nav">
                  <li><Link to="/selg">Selg bok</Link></li>
                  <li><Link to="/bytt">Bokbytteklubben</Link></li>
                  <li><Link to="/login">Login</Link></li>
                  <li><Link to="/registrer">Registrer</Link></li>
              </ul>
          </div>
      </nav>
    </header>

  );
};

//export default: Tenk på det som en "return" statement
export default Nav;
