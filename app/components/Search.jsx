var React = require('react');


/*
Siden funktionen ikke forhodler seg til state kan vi bruke en enkel funktion istendefor en React klasse.
Dette har flere fordeler. Ytelse, kodeforståelse og testbarhet for å nevne noen.

class er et reservert ord i jsx, så CSS class byttes derfor ut med "className" 
*/
const Search = () =>{
  return(
    <div className="flexsearch">
    		<div className="flexsearch--wrapper">
    			<form className="flexsearch--form" action="#" method="post">
    				<div className="flexsearch--input-wrapper">
    					<input className="flexsearch--input" type="search" placeholder="Søk"></input>
    				</div>
    				<input className="flexsearch--submit" type="submit" value="&#10140;"></input>
    			</form>
    		</div>
    </div>
  );
};

//export default: Tenk på det som en "return" statement
export default Search;
