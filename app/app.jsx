import React, { Component } from 'react';
var ReactDOM = require('react-dom');
var {Route, Router, IndexRoute, hashHistory} = require('react-router');

//Egne komponenter
import Nav from 'Nav';
import WelcomeText from 'WelcomeText';
import Search from 'Search';


/*
React klasse som til slutt ender opp i DOM.
Fungerer som en kontainer komponent for hele siden og inneholder andre mindre kompoenter spesifisert med <component/>
Vil i fremtiden holde staten til applikasjonen 
*/
var App = React.createClass({
  render : function () {
    return (
      <div>
        <Nav/>
        <Search/>
        <WelcomeText/>
      </div>
    );
  }
});



/*
Sender all data inn i document object model (DOM) i browseren.
Router history: React komponent som holder oversikt over state i historikken i tillegg til URL.
Hvis i root'/', render App.render()
*/
ReactDOM.render(

  <Router history={hashHistory}>

    <Route path="/" component={App}>
    </Route>
  </Router>,
  document.getElementById('app')
);
